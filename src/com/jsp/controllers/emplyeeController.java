package com.jsp.controllers;

import java.io.IOException;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jsp.dao.EmployeeDAO;
import com.jsp.dao.EmployeeDAOIMPL;
import com.jsp.dao.ReportDAO;
import com.jsp.entity.Emplyee;
import com.jsp.entity.Report;


public class emplyeeController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    RequestDispatcher dispatcher = null;
    EmployeeDAO employeeDAO = null;
    ReportDAO reportDAO = null;


    public emplyeeController() {
        employeeDAO = new EmployeeDAOIMPL();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");

        if(action == null) {
            action = "LIST";
        }

        switch(action) {

            case "LIST":
                listEmployee(request, response);
                break;

            case "EDIT":
                getSingleEmployee(request, response);
                break;

            case "DELETE":
                deleteEmployee(request, response);
                break;


            default:
                listEmployee(request, response);
                break;

        }

    }

    private void deleteEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");

        if(employeeDAO.delete(Integer.parseInt(id))) {
            request.setAttribute("NOTIFICATION", "Employee Deleted Successfully!");
        }

        listEmployee(request, response);
    }

    private void getSingleEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        String id = request.getParameter("id");

        Emplyee theEmployee = employeeDAO.get(Integer.parseInt(id));

        request.setAttribute("employee", theEmployee);

        dispatcher = request.getRequestDispatcher("/views/employee-form.jsp");

        dispatcher.forward(request, response);
    }

    private void listEmployee(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Emplyee> theList = employeeDAO.get();

        request.setAttribute("list", theList);

        dispatcher = request.getRequestDispatcher("/views/employee-list.jsp");

        dispatcher.forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");

        Emplyee e = new Emplyee();
        e.setName(request.getParameter("name"));
        e.setDepartment(request.getParameter("department"));
        e.setDob(request.getParameter("dob"));

        if(id.isEmpty() || id == null) {

            if(employeeDAO.save(e)) {
                request.setAttribute("NOTIFICATION", "Employee Saved Successfully!");
            }

        }else {

            e.setId(Integer.parseInt(id));
            if(employeeDAO.update(e)) {
                request.setAttribute("NOTIFICATION", "Employee Updated Successfully!");
            }

        }

        listEmployee(request, response);
    }

}



