package com.jsp.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionUtil
{
    //Define DataBase Proparties
    private static String url = "jdbc:mysql://localhost:3306/employeeddirectory";
    private static String Driver = "com.mysql.jdbc.Driver";
    private static String UserName = "root";
    private static String Password = "";
    private static Connection connection = null;

    // Define the Static Mehod

    public static Connection openConnectionn()

    {
//check the connection
if (connection != null)
{
return connection;
}
else
{

    try {

        //check the driver
        Class.forName(Driver);
        //Get teh connection
        connection = DriverManager.getConnection(url,UserName,Password);
    }
    catch (Exception e) {
        e.printStackTrace();
    }

}

        //return the connection
        return connection;

    }
}
