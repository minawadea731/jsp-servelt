package com.jsp.dao;

import com.jsp.entity.Emplyee;

import java.util.List;

public interface EmployeeDAO
{
    List<Emplyee> get();

    Emplyee get(int id);


    boolean save(Emplyee employee);

    boolean delete(int id);

    boolean update(Emplyee employee);
}
