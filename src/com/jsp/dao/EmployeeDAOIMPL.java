package com.jsp.dao;

import com.jsp.entity.Emplyee;
import com.jsp.util.DBConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAOIMPL implements EmployeeDAO {
    Connection connection = null;
    ResultSet resultSet = null;
    Statement statement = null;
    PreparedStatement preparedStatement = null;

    @Override
    public List<Emplyee> get() {

        List<Emplyee> list = null;
        Emplyee employee = null;

        try {

            list = new ArrayList<Emplyee>();
            String sql = "SELECT * FROM tbl_employee";
            connection = DBConnectionUtil.openConnectionn();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                employee = new Emplyee();
                employee.setId(resultSet.getInt("id"));
                employee.setName(resultSet.getString("name"));
                employee.setDepartment(resultSet.getString("department"));
                employee.setDob(resultSet.getString("dob"));
                list.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Emplyee get(int id) {
        Emplyee employee = null;
        try {
            employee = new Emplyee();
            String sql = "SELECT * FROM tbl_employee where id=" + id;
            connection = DBConnectionUtil.openConnectionn();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                employee.setId(resultSet.getInt("id"));
                employee.setName(resultSet.getString("name"));
                employee.setDepartment(resultSet.getString("department"));
                employee.setDob(resultSet.getString("dob"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employee;
    }

    @Override
    public boolean save(Emplyee e) {
        boolean flag = false;
        try {
            String sql = "INSERT INTO tbl_employee(name, department, dob)VALUES"
                    + "('" + e.getName() + "', '" + e.getDepartment() + "', '" + e.getDob() + "')";
            connection = DBConnectionUtil.openConnectionn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
            flag = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean delete(int id) {
        boolean flag = false;
        try {
            String sql = "DELETE FROM tbl_employee where id=" + id;
            connection = DBConnectionUtil.openConnectionn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean update(Emplyee employee) {
        boolean flag = false;
        try {
            String sql = "UPDATE tbl_employee SET name = '" + employee.getName() + "', "
                    + "department = '" + employee.getDepartment() + "', dob = '" + employee.getDob() + "' where id=" + employee.getId();
            connection = DBConnectionUtil.openConnectionn();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }
}